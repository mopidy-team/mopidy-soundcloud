Metadata-Version: 2.1
Name: Mopidy-SoundCloud
Version: 3.0.2
Summary: Mopidy extension for playing music from SoundCloud
Home-page: https://github.com/mopidy/mopidy-soundcloud
Author: Janez Troha
Author-email: dz0ny@ubuntu.si
License: MIT
Classifier: Environment :: No Input/Output (Daemon)
Classifier: Intended Audience :: End Users/Desktop
Classifier: License :: OSI Approved :: MIT License
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Topic :: Multimedia :: Sound/Audio :: Players
Requires-Python: >=3.7
Provides-Extra: lint
Provides-Extra: test
Provides-Extra: dev
License-File: LICENSE

*****************
Mopidy-SoundCloud
*****************

.. image:: https://img.shields.io/pypi/v/Mopidy-SoundCloud
    :target: https://pypi.org/project/Mopidy-SoundCloud/
    :alt: Latest PyPI version

.. image:: https://img.shields.io/github/workflow/status/mopidy/mopidy-soundcloud/CI
    :target: https://github.com/mopidy/mopidy-soundcloud/actions
    :alt: CI build status

.. image:: https://img.shields.io/codecov/c/gh/mopidy/mopidy-soundcloud
    :target: https://codecov.io/gh/mopidy/mopidy-soundcloud
    :alt: Test coverage

`Mopidy <https://mopidy.com/>`_ extension for playing music from
`SoundCloud <https://soundcloud.com>`_.


Maintainer wanted
=================

Mopidy-SoundCloud is currently kept on life support by the Mopidy core
developers. It is in need of a more dedicated maintainer.

If you want to be the maintainer of Mopidy-SoundCloud, please:

1. Make 2-3 good pull requests improving any part of the project.

2. Read and get familiar with all of the project's open issues.

3. Send a pull request removing this section and adding yourself as the
   "Current maintainer" in the "Credits" section below. In the pull request
   description, please refer to the previous pull requests and state that
   you've familiarized yourself with the open issues.

As a maintainer, you'll be given push access to the repo and the authority to
make releases to PyPI when you see fit.


Installation
============

Install by running::

    sudo python3 -m pip install Mopidy-SoundCloud

See https://mopidy.com/ext/soundcloud/ for alternative installation methods.


Configuration
=============

#. You must register for a user account at https://soundcloud.com/

#. You need a SoundCloud authentication token for Mopidy from
   https://mopidy.com/authenticate

#. Add the authentication token to the ``mopidy.conf`` config file::

    [soundcloud]
    auth_token = 1-1111-1111111
    explore_songs = 25

#. Use ``explore_songs`` to restrict the number of items returned.


Troubleshooting
===============

If you're having trouble with audio playback from SoundCloud, make sure you
have the "ugly" plugin set from GStreamer installed for MP3 support. The
package is typically named ``gstreamer1.0-plugins-ugly`` or similar, depending
on OS and distribution. The package isn't a strict requirement for Mopidy's
core, so you may be missing it.


Project resources
=================

- `Source code <https://github.com/mopidy/mopidy-soundcloud>`_
- `Issue tracker <https://github.com/mopidy/mopidy-soundcloud/issues>`_
- `Changelog <https://github.com/mopidy/mopidy-soundcloud/releases>`_


Credits
=======

- Original author: `Janez Troha <https://github.com/dz0ny>`_
- Current maintainer: None. Maintainer wanted, see section above.
- `Contributors <https://github.com/mopidy/mopidy-soundcloud/graphs/contributors>`_
